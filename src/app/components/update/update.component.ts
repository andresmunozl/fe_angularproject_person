import { Component, OnInit } from '@angular/core';
import { PersonServicesService, Person } from '../../services/person-services.service'
import { Router,ActivatedRoute } from '@angular/router'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  person: Person = {
    id_person: null,
    number_identification: '',
    full_name: '',
    full_last_name: '',
    birthdate: '',
    sex: '',
    status: '',
  }
  idPerson:any;
  constructor(private personService: PersonServicesService, private router: Router, private activeRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.idPerson = this.activeRoute.snapshot.params['id'];

    if(this.idPerson){
      this.personService.getPerson(this.idPerson).subscribe(
        (res:any)=>{
          this.person = res[0];
        },
        err=>console.log(err)
      );
    }else{
      Swal.fire({
        title: "Persona No encontrada",
        text: "vuelva a intentarlo, parece que la persona seleccionada cambio recientemente de estado.",
        icon: "info",
        confirmButtonText: "Aceptar",
        confirmButtonColor: "#2d2d2d",
      });
      this.router.navigate(['/home'])
    }
  }

  storePeople() {
    if (
      (this.person.number_identification == '') ||
      (this.person.full_name == '') ||
      (this.person.full_last_name == '') ||
      (this.person.birthdate == '') ||
      (this.person.sex == '')
    ) {
      Swal.fire({
        title: "Persona agregada",
        text: "campos no estan llenos, verifica y vuelve a intentarlo.",
        icon: "info",
        confirmButtonText: "Aceptar",
        confirmButtonColor: "#2d2d2d",
      });
    } else {

      delete this.person.id_person;
      this.personService.updatePerson(this.idPerson, this.person).subscribe(
        (response: any) => {
          Swal.fire({
            title: "Persona Actualizada",
            text: "Se ha modificado los datos de la persona exitosamente.",
            icon: "success",
            confirmButtonText: "Aceptar",
            confirmButtonColor: "#2d2d2d",
          });
          this.router.navigate(['/home'])
        },
        (err:any) => {
          console.log(err);

        }
      )
    }
  }

}

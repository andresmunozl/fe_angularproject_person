import { Component, OnInit } from '@angular/core';
import { PersonServicesService, Person } from '../../services/person-services.service'
import { Router,ActivatedRoute } from '@angular/router'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  person: Person = {
    id_person: null,
    number_identification: '',
    full_name: '',
    full_last_name: '',
    birthdate: '',
    sex: '',
    status: '',
  }
  idPerson:any;
  constructor(private personService: PersonServicesService, private router: Router, private activeRoute:ActivatedRoute) { }

  ngOnInit(): void {

    this.idPerson = this.activeRoute.snapshot.params['id'];

    if(this.idPerson){
      this.personService.getPerson(this.idPerson).subscribe(
        (res:any)=>{
          this.person = res[0];
        },
        err=>console.log(err)
      );
    }else{
      Swal.fire({
        title: "Persona No encontrada",
        text: "vuelva a intentarlo, parece que la persona seleccionada cambio recientemente de estado.",
        icon: "info",
        confirmButtonText: "Aceptar",
        confirmButtonColor: "#2d2d2d",
      });
      this.router.navigate(['/home'])
    }

  }

  homePersonRedirect() {
    this.router.navigate(['/home'])
  }

}

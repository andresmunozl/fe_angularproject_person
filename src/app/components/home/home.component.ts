import { Component, OnInit } from '@angular/core';
import { PersonServicesService, Person } from '../../services/person-services.service'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  listPeople: Person[] = [];
  listPeopleOriginal: Person[] = [];
  word: any = ""
  constructor(private personService: PersonServicesService, private router: Router) { }

  ngOnInit(): void {
    this.getPeople()
  }

  getPeople() {
    this.personService.getPeople().subscribe(
      (response: any) => {
        // console.log(response);
        this.listPeople = response;
        this.listPeopleOriginal = response;
      },
      (err: any) => {
        console.log(err);

      }
    )
  }

  serachPeople() {
    // console.log(this.listaPic);
    let listPersonFilter: any[] = [];
    // this.listaConvocatoriasFiltrado = []

    for (let person of this.listPeople) {
      let identification: any = person.number_identification;
      let name: any = person.full_name;
      let lasttName: any = person.full_last_name;
      let bithdate: any = person.birthdate;


      if (
        identification.indexOf(this.word.toLowerCase()) >= 0 ||
        name.indexOf(this.word.toLowerCase()) >= 0 ||
        lasttName.indexOf(this.word.toLowerCase()) >= 0 ||
        bithdate.indexOf(this.word.toLowerCase()) >= 0

      ) {
        listPersonFilter.push(person);
      }
      this.listPeople = listPersonFilter;
      if (this.word == 0) {
        this.getPeople();
      }

    }
  }

  deletePerson(id: any) {
    Swal.fire({
      title: "Eliminar Persona",
      text: "seguro deseas borrar esta persona?",
      icon: "warning",
      confirmButtonText: "Eliminar",
      confirmButtonColor: "#dc3545",
      cancelButtonColor: "#2d2d2d",
      cancelButtonText: "Cancelar",
      showCancelButton: true,
    }).then((respuesta) => {
      if (respuesta.isConfirmed) {
        this.personService.deletePerson(id).subscribe(
          (response: any) => {
            // se eliminó
            Swal.fire({
              title: "Persona borrada exitosamente",
              icon: "success",
              confirmButtonText: "Aceptar",
              confirmButtonColor: "#2d2d2d",
            });

          },
          (err: any) => {
            console.log(err);

          }
        )

      }
    });
    this.getPeople();

  }

  updatePersonRedirect(id: any) {
    this.router.navigate(['update/' + id])
  }

  storePersonRedirect() {
    this.router.navigate(['store'])
  }

  showPersonRedirect(id: any) {
    this.router.navigate(['/show/' + id])
  }

}

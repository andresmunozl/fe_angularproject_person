import { Component, OnInit } from '@angular/core';
import { PersonServicesService, Person } from '../../services/person-services.service'
import { Router } from '@angular/router'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  person: Person = {
    id_person: null,
    number_identification: '',
    full_name: '',
    full_last_name: '',
    birthdate: '',
    sex: '',
    status: '',
  }
  constructor(private personService: PersonServicesService, private router: Router) { }

  ngOnInit(): void {
  }

  storePeople() {
    if (
      (this.person.number_identification == '') ||
      (this.person.full_name == '') ||
      (this.person.full_last_name == '') ||
      (this.person.birthdate == '') ||
      (this.person.sex == '')
    ) {
      Swal.fire({
        title: "Campos vacio",
        text: "campos no estan llenos, verifica y vuelve a intentarlo",
        icon: "info",
        confirmButtonText: "Aceptar",
        confirmButtonColor: "#2d2d2d",
      });
    } else {

      delete this.person.id_person;
      this.personService.storePerson(this.person).subscribe(
        (response: any) => {
          // console.log(response);
          Swal.fire({
            title: "Persona agregada",
            text: "Se ha registrado la persona exitosamente.",
            icon: "success",
            confirmButtonText: "Aceptar",
            confirmButtonColor: "#2d2d2d",
          });
          this.router.navigate(['/home'])
        },
        (err: any) => {
          Swal.fire({
            title: "Persona agregada",
            text: err,
            icon: "success",
            confirmButtonText: "Aceptar",
            confirmButtonColor: "#2d2d2d",
          });

        }
      )
    }
  }

}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonServicesService {

  baseUrl = '/api/person';
  constructor(private http: HttpClient) {

  }

  getPeople()
  {
    return this.http.get(this.baseUrl);
  }

  getPerson(id:any)
  {
    return this.http.get(this.baseUrl + '/'+ id);
  }

  storePerson(person:Person)
  {
    return this.http.post(this.baseUrl, person);
  }

  deletePerson(id:any){
    return this.http.delete(this.baseUrl+'/'+id);
  }

  updatePerson(id:any, person:Person){
    return this.http.put(this.baseUrl+'/'+id, person);
  }
}

export interface Person{
  id_person?:any;
  number_identification?:string;
  full_name?:string;
  full_last_name?:string;
  birthdate?:string;
  sex?:string;
  status?:string;
}

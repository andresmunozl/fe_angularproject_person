import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//files created by Andres
import { StoreComponent } from './components/store/store.component';
import { HomeComponent } from './components/home/home.component';
import { UpdateComponent } from './components/update/update.component';
import { ShowComponent } from './components/show/show.component';

const routes: Routes = [

  { path: '', redirectTo: '/home', pathMatch: 'full' },//when there aren't url, so default redirect to home
  { path: 'home', component: HomeComponent },
  { path: 'store', component: StoreComponent },
  { path: 'update/:id', component: UpdateComponent },
  { path: 'show/:id', component: ShowComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
